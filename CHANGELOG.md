# [](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/compare/v1.0.5...v) (2025-03-01)



## [1.0.5](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/compare/v1.0.4...v1.0.5) (2025-02-27)


### Bug Fixes

* rebranding applied [skip ci] ([50abb05](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/commit/50abb05e272058fcd8f30b58e8fd4061cb34df4d))
* renovate json updated [skip ci] ([12f124d](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/commit/12f124dd76bc045eb4dbe11e76d3307447bd1df6))
* renovate json updated [skip ci] ([d0f7b5a](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/commit/d0f7b5a1ff3281f03eb50715072bb0517ffd296e))



## [1.0.1](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/compare/v1.0.0...v1.0.1) (2025-02-19)


### Bug Fixes

* rebranding applied ([be3dfd3](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/commit/be3dfd3d71709b207e5140f81b53d2c319135947))



# [1.0.0](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/compare/109fc94df6dc8169047758d6ff3f88d7829523de...v1.0.0) (2025-02-12)


### Bug Fixes

* gitlab-ci.yml job error fixed ([109fc94](https://gitlab.com/pss-x/support/gitlab-ci-templates/release-cli/commit/109fc94df6dc8169047758d6ff3f88d7829523de))



