# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue with the owners of this repository before making a change. 

Please check [Issues](https://gitlab.com/pss-x/gitlab-profile/-/issues) and look for unassigned ones or create a new one.

Please note we have a code of conduct.

Working together in an open and welcoming environment is the foundation of our success, so please respect our [Code of Conduct](#code-of-conduct).

## General Code Contribution Guidelines

### Merge Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.

2. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.

3. Increase the version numbers in any examples files and the README.md to the new version that Merge Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).

4. You may merge the Merge Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.

### Workflow

We use the [GitHub Flow](https://docs.github.com/en/get-started/quickstart/github-flow). The mentioned links from GitHub are the recommended docs to read and understand the workflows.

### Branch Naming Conventions

We follow very basic rules for naming Git branches:

* Branch names are prefixed with the commit type
* Branch names are all lower case and words are connected by dashes, also known as kebab-case
* Branch names describe the change

Examples: feat/add-new-component, fix/bug-in-page-load, docs/update-user-guide

### Commit Message Format

We follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification to format Git commit messages.
This allows us to efficiently create releases based on [semantic versioning](https://semver.org) and automatically generate the changelog

### License Headers

The only license header we need for a software written by GridLab is:

```txt
Copyright YEAR, GridLab
```
The YEAR shall reflect the creation year of the code.

We appreciate if you share your code within the **GridLab family** and help us to build the **corporate memory** of code. If you do so please add the following license header for your project:

```txt
Copyright YEAR, GridLab

Licensed under the GNU Lesser General Public License (LGPL)
```

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community. 
Examples of representing a project or community include using an official project e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event.
Representation of a project may be further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by using the inbuilt *Report Abuse* function on the platform itself or by sending an e-mail to [denizraifdurmaz@gmail.com](mailto:denizraifdurmaz@gmail.com).
All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The project team is obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/