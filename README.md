# GitLab Release command-line tool

> [Introduced](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6) in GitLab 12.10.

GitLab Release command-line tool is an application written in [Golang](https://golang.org/) to interact with [GitLab's Releases API](https://docs.gitlab.com/ee/api/releases/index.html) through the command line and through GitLab CI/CD's configuration file, [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/#release).
The minimum supported Go version is v1.13.

It consumes instructions in the `:release` node of the `.gitlab-ci.yml` to create a Release object in GitLab Rails.

The GitLab Release CLI is a decoupled utility that may be called by the GitLab Runner, by a third-party CI or directly from the command line.
It uses the CI `Job-Token` to authorize against the GitLab Rails API, which is passed to it by the GitLab Runner.

The CLI can also be called independently, and can still create the Release via Rails API
if the `Job-Token` and correct command line params are provided.

## Usage

To get started, open your project in a terminal and run `release-cli help`
for usage options. The output will be:

```plaintext
NAME:
   release-cli - A CLI tool that interacts with GitLab's Releases API

USAGE:
   help [global options] command [command options] [arguments...]

VERSION:
   0.13.0

DESCRIPTION:
   CLI tool that interacts with GitLab's Releases API https://docs.gitlab.com/ee/api/releases/.
   
   All configuration flags will default to GitLab's CI predefined environment variables (https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
   To override these values, use the [GLOBAL OPTIONS].
   
   Get started with release-cli https://gitlab.com/gitlab-org/release-cli.

AUTHOR:
   GitLab Inc. <support@gitlab.com>

COMMANDS:
   create   Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release
   get      Get a Release by tag name using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/index.html#get-a-release-by-a-tag-name
   update   Update a release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#update-a-release
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --server-url value                 The base URL of the GitLab instance, including protocol and port, for example https://gitlab.example.com:8080 [$CI_SERVER_URL]
   --job-token value                  Job token used for authenticating with the GitLab Releases API (default:  ) [$CI_JOB_TOKEN]
   --project-id value                 The current project's unique ID; used by GitLab CI internally [$CI_PROJECT_ID]
   --timeout value                    HTTP client's timeout in Go's duration format https://golang.org/pkg/time/#ParseDuration (default: 30s) [$RELEASE_CLI_TIMEOUT]
   --private-token value              Private token used for authenticating with the GitLab Releases API, requires api scope https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html, overrides job-token (default:  ) [$GITLAB_PRIVATE_TOKEN]
   --additional-ca-cert-bundle value  Configure a custom SSL CA certificate authority, can be a path to file or the content of the certificate [$ADDITIONAL_CA_CERT_BUNDLE]
   --insecure-https                   Set to true if you want to skip the client verifying the server's certificate chain and host name (default: false) [$INSECURE_HTTPS]
   --debug                            Set to true if you want extra debug output when running release-cli (default: false) [$DEBUG]
   --help, -h                         Show help (default: false)
   --version, -v                      Print the version (default: false)
```
This command uses the [Create a Release](https://docs.gitlab.com/ee/api/releases/) API.

```plaintext
NAME:
   help create - Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release

USAGE:
   help create [command options] [arguments...]

OPTIONS:
   --name value               The release name
   --description value        The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string
   --tag-name value           The tag the release will be created from [$CI_COMMIT_TAG]
   --tag-message value        Message to use if creating a new annotated tag
   --ref value                If tag_name doesn’t exist, the release will be created from ref; it can be a commit SHA, another tag name, or a branch name [$CI_COMMIT_SHA]
   --assets-link value        JSON string representation of an asset link (or an array of asset links); (e.g. --assets-link='{"name": "Asset1", "url":"https://example.com/some/location/1", "link_type": "other", "filepath": "xzy" } or --assets-link='[{"name": "Asset1", "url":"https://example.com/some/location/1"}, {"name": "Asset2", "url":"https://example.com/some/location/2"}]')
   --milestone value          List of the titles of each milestone the release is associated with (e.g. --milestone "v1.0" --milestone "v1.0-rc)"; each milestone needs to exist
   --released-at value        The date when the release will be/was ready; defaults to the current time; expected in ISO 8601 format (2019-03-15T08:00:00Z)
   --help, -h                 Show help (default: false)
```

The `update` command uses the [Update a release](https://docs.gitlab.com/ee/api/releases/#update-a-release) endpoint from the Releases API:

```plaintext
NAME:
   help update - Update a release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#update-a-release

USAGE:
   help update [command options] [arguments...]

OPTIONS:
   --tag-name value     The Git tag the release is associated with [$CI_COMMIT_TAG]
   --name value         The release name
   --description value  The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string
   --milestone value    List of the titles of each milestone the release is associated with (e.g. --milestone "v1.0" --milestone "v1.0-rc)"; each milestone needs to exist. Pass an empty string to remove all milestones from the release.  (accepts multiple inputs)
   --released-at value  The date when the release will be/was ready; defaults to the current time; expected in ISO 8601 format (2019-03-15T08:00:00Z)
   --help, -h           Show help (default: false)
```

## Referencing CI Template

[Gitlab CI includes](https://docs.gitlab.com/ee/ci/yaml/#include) are used for a seamless pipeline intgration.

Following code snippet should be inserted on top of the `.gitlab-ci.yml` file for global usage:

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/release-cli'
    ref: 'main' # select desired version here
    file: '/templates/release.yml'
```

RECOMMENDED: use stable version like tag as `ref`.

### Inputs

The “specs” attribute has been used to identify stage names in `release.yml` file

| Input | Description | Default Value | 
| ------------- | ------------- | -------- |
| stage | The stage name where you want the release job to be added. | release | 

## Pipeline stages

Stage which is need to run for creatin Gitlab Release

```yaml
stages:
  - release
```

## Getting started

1.  Generate a Token with "api" scope via <br> [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) 
    <br> Project Access Token ( gitlab.com -> your project -> settings -> project access token)

2. Set Environmental Variable in your Project/Group (Settings -> CI/CD / Variables)<br>
   Variable Name **GL_TOKEN** and your access token as value

3. In order to execute release-cli, the following variables must be defined.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | VERSION | x | This value is going to use for TAG and RELEASE NAME, prefixed with `v` | v{VERSION} | 
| V | VERSION_FILE | | The file name where version information going to store | VERSION |
| V | CHANGELOG_FILE | | The file name where generated changelogs going to store | CHANGELOG.md |
| V | DESCRIPTION | | The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string | |

### Gitlab Secret Variables

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | GL_TOKEN | x | A GitLab [personal access token.](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) |

## Release assets

A release contains the following types of assets:
* Source code
* Link

Developers can easily track the source code and artifact that was created in the build stage. This is useful for distributing software releases or other types of files to users.

Each link as an asset has the following attributes:

| Attribute | Description  | Required |
| ------------- | ------------- | -------- |
| name | The name of the link | x |
| url | The URL to download a file | x |
| filepath | The redirect link to the url | |
| link_type | The content kind of what users can download via url | |

## Permanent links to release assets

Job artifacts that can also be used for attaching binary files to an individual release entry.

You basically need to:
* Fix the artifact
* Store package names with relative jobids

```yml
.nuget:pack:
  ...
  script:
    - ...
    - cp $ARTIFACT_DIRECTORY/* $NUGET_DIRECTORY
    - export NUGET_CONFIG_PACKAGE_NAME=$(find $ARTIFACT_DIRECTORY -name '*.nupkg' | sed 's#.*/##; s#[.][^.]*$##')
    - cat > ASSETS 
    - echo "$NUGET_CONFIG_PACKAGE_NAME,$CI_JOB_ID" >> ${CI_PROJECT_DIR}/ASSETS
    - ...
  artifacts:
    paths:
      - $ARTIFACT_DIRECTORY/*
      - ASSETS 
    expire_in: never
```

Alternatively you can use generic packages to store any artifacts from a release or tag pipeline.

```yml
  ...
  variables:
    PUSH_TO_GENERIC_REGISTRY: "true"
  ... 
  script:
    - ...
    - cat > ASSETS 
    - echo "$NUGET_CONFIG_PACKAGE_NAME,$CI_JOB_ID" >> ${CI_PROJECT_DIR}/ASSETS     
    - |
      if [[ "$PUSH_TO_GENERIC_REGISTRY" == "true" ]] || [[ "$PUSH_TO_GENERIC_REGISTRY" == "yes" ]] || [[ "$PUSH_TO_GENERIC_REGISTRY" == "1" ]]; then
        curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "$ARTIFACT_DIRECTORY/$NUGET_CONFIG_PACKAGE_NAME" "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/generic/$NUGET_CONFIG_PACKAGE_NAME/$NUGET_CONFIG_PACKAGE_VERSION"
      fi
  artifacts: 
    paths:
      - $ARTIFACT_DIRECTORY/*
      - ASSETS
    expire_in: 1 days
```

NOTE: Directly attaching job artifacts links to a release is not recommended, because artifacts are ephemeral and are used to pass data in the same pipeline.

## Have a Problem or Suggestion ?

Please see a list of features and bugs under issues If you have a problem which is not listed there, please let us known.   