using GridLab.PSSX.WebApi.Controllers;
using Microsoft.Extensions.Logging.Abstractions;

namespace GridLab.PSSX.WeatherForecast.TestBase
{
  public class WeatherForecastControllerTests
  {
    [Fact]
    public void Should_Return_A_List_Of_Values()
    {
      // Arrange
      var logger = new NullLogger<WeatherForecastController>();
      var service = new WeatherForecastController(logger);

      // Act
      var result = service.Get();

      // Assert
      Assert.NotNull(result);
    }
  }
}